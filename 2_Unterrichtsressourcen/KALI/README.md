# Inhalt der Kompetenz A

[[_TOC_]]

## Uebersicht

Kali Linux is an open-source, Debian-based Linux distribution geared towards various information security tasks, such as Penetration Testing, Security Research, Computer Forensics and Reverse Engineering.

## Installation
Kali Linux gibts als Docker, VMWare image oder ISO. Alles unter [Kali.org](https://www.kali.org/)

## Umgebung
Um Ethical Hacking (oder Penetrationtest oder...) zu betreiben empfiehlt es sich in einem Labor zu üben. Zum Glück sind die meisten Schwachstellen nach deren Entdeckung behoben. 

In diesem Kurs wird eine bewusst schwache VM (Metaexploitable2) eingesetzt um einige techniken und Beispiele zu zeigen. [Download](https://sourceforge.net/projects/metasploitable/files/Metasploitable2/)

Da sich die Installation von Umgebung zu Umgebung verändert, wird auf eine genaue Anleitung hier verzichtet.

Am besten ist Metasploitable nicht vom Internet zugänglich, und in einem privaten Netzwerk "nur" mit dem Kali Linux verbunden.

## Uebung
### Vertraut machen mit Kali Linux
Auch hier gibt es zahlreiche Tutorials. Zum Beispiel: 
- [Basics for Beginners](https://www.youtube.com/watch?v=Yg4tV98y69I)
- [KAli Linux Course 2023](https://www.youtube.com/watch?v=ty8bEFuVM-I) 

### Netzwerk Scan
Bevor wir angreifen müssen wir Systeme und SChwachstellen finden. 
- [How to find open Ports](https://www.youtube.com/watch?v=6rG9gigN4c8)

### Passwort Brutforce für mySql
Vielleicht klappts schon selber, ansonsten ist diese Tutorial zu empfehlen.
- [How to Hack Mysql on Metasploitable](https://www.youtube.com/watch?v=xHKKWeR7vZw)
-Auch SSH oder FTP können mit Passwort Brutforce gehackt werden.

### System übernahme mittel CGI PHP exploit
Auch hier gibt es zahlreiche Tutorials. Eines davon: 
- [PHP CGI Argument Injection](https://www.youtube.com/watch?v=Bo4A7IFJ2HQ)

### Netzwerk hack
Dies kann zur Zeit im TBZ Lab nicht ausgeführt werden. Trotzdem hier zwei beeindruckende Hacks, weil nachlässig konfiguriert wurde.
- [VTP Attacke](https://www.youtube.com/watch?v=K1bMSPje6pw)
- [Cracking WIFI](https://www.youtube.com/watch?v=WfYxrLaqlN8)
