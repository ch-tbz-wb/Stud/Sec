![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# CISSP

## Kurzbeschreibung des Moduls 

CISSP ist die Zertifizierung für ganzheitliche Sicherheit. Sie umfasst neu 8 Domainen. Sie wird von der ISC2.org verwaltet.

- Security and Risk Management
- Asset Security
- Security Architecture and Engineering
- Communication and Network Security
- Identity and Access Management
- Security Assestment and Testing
- Security Operations
- Software Development Security

Alle diese Domainen werden in einem Kurzvideo vorgestellt. Im Sinne eines Gruppenpuzzles wird jeder eine Domaine vertieft erkunden und der Gruppe präsentieren

Links:
- CISSP short Intro Videos: [here](https://www.youtube.com/watch?v=vV9ARroxRsQ&list=PLOWdy-NBQHJuF-DLGbvBkQ8eXjXzeuTXV)
- CISSP full: [here](https://www.youtube.com/watch?v=_nyZhYnCNLA)
- ISC Orgnaisation [here](https://www.isc2.org/certifications/cissp)

