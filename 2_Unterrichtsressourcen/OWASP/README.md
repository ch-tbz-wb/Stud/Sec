# Inhalt der Kompetenz A

[[_TOC_]]

*Ersetzen mit einer allgemeinen Beschreibung mit evtl. vorhandenem Big Picture oder Einführungsfilm*

## Übersicht
OWASP spielt eine entscheidende Rolle bei der Sensibilisierung für Sicherheitsrisiken von Webanwendungen und bietet wertvolle Ressourcen, Tools, Dokumentationen und Best Practices, um den zunehmenden Herausforderungen der Sicherheit von Webanwendungen zu begegnen. OWASP hilft Entwicklern, Sicherheitsexperten und Organisationen, potenzielle Bedrohungen zu verstehen und bewährte Sicherheitspraktiken anzuwenden.

OWASP führt eine Liste der zehn kritischsten Sicherheitsrisiken für Webanwendungen sowie effektive Prozesse, Verfahren und Kontrollen zu deren Eindämmung. OWASP bietet auch eine Liste der 10 kritischsten API-Sicherheitsrisiken, um die an der API-Entwicklung und -Wartung Beteiligten zu schulen und das Bewusstsein für häufige Schwachstellen der API-Sicherheit zu schärfen.

Die OWASP-Community ermutigt Einzelpersonen und Organisationen, sich an ihren Projekten und Ressourcen zu beteiligen. Dieser auf Zusammenarbeit und Umfragen basierende Ansatz ermöglicht es der Community, das kollektive Wissen und die Expertise ihrer Mitglieder zu nutzen, was zu umfassenden und aktuellen Ressourcen führt.

Es gibt Sicherheitsrisiken, die sowohl Anwendungen als auch APIs gemeinsam sind und bei der Implementierung von Sicherheitslösungen berücksichtigt werden sollten. Zum Beispiel: 

Schwache Authentifizierungs-/Autorisierungskontrollen
Fehlkonfiguration
Missbräuchliche Nutzung der Geschäftslogik (Credential Stuffing, Account Takeover)
Serverseitige Anfragenfälschung (SSRF, Server-Side Request Forgery).


## Uebungen
OWASP WebGoat ist ein Tool das bewusst unsicher ist.
- Wenn noch nicht vorhanden Installiert [Docker:](https://docs.docker.com/desktop/install/windows-install/)
- Installiert [WebGoat:](https://owasp.org/www-project-webgoat/)

- Die Aufgaben sind schwierigkeitsgrad optimiert, Da dies ein Opensource Tool ist, gibt es sehr gute Walkthrough Videos.

Ums tets up to date zu sein, empfehle ich jeweils eine [Suche:](https://www.google.com/search?q=webgoat+walkthrough&rlz=1C1CSMH_deCH1033CH1033&oq=Webgoat+wa&gs_lcrp=EgZjaHJvbWUqCQgAEAAYExiABDIJCAAQABgTGIAEMgYIARBFGDkyCggCEAAYExgWGB4yCggDEAAYExgWGB4yDAgEEAAYChgTGBYYHjIKCAUQABgTGBYYHjIKCAYQABgTGBYYHjIKCAcQABgTGBYYHjIKCAgQABgTGBYYHjIKCAkQABiABBiiBKgCALACAQ&sourceid=chrome&ie=UTF-8)