![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# DSEC - Datensicherheit

## Kurzbeschreibung des Moduls 

Sicherheit geht vor! Diesen Satz hören wir alle seit wir Kinder sind und haben es auch nie in frage gestellt. Ohne Sicherheit geht nichts. Anlässe werden abgesagt, Wahlen verschoben, Flüge storniert. Ganze Bevölkerungen werden während einer Pandemie eingesperrt oder noch aktueller, die Versorgungssicherheit des Stromnetzes, an welcher mit höchster Priorität gearbeitet wird. 

Doch was sicher, verstehen wir alle das gleiche unter Sicherheit und warum haben wir trotz "Sicherheit geht vor" tortzdem Stromausfälle, Medikamentenengpässe und die Pandemie hat tortz aller Massnahmen unser Leben dominiert. Die absolute Sicherheit gibt es nicht, und wenn es diese gäbe, wäre es es der Preis nicht wert. 

In diesem Modul schauen wir IT Sicherheit von verschiedenen Blickwinkel an. Wir behandeln das CIA Dreieck (Confidentiality, Integrity und Availability) und Ihre Asuwirkungen auf IT Prozesse. Wir schauen aktuelle Beispiele an und ordnen diese ein, bzw. prüfen ob wir diese in die eine oder andere Richtung verbessern könnten und wie diese aussehen.

In einem zweiten Teil streifen wir kurz die 8 Dimensionen der CISSP Zertifizierung. Nur in Kompination gelingt uns IT-Sicherheit zu definieren. Diese 8 Dimensionen sind: 
- Sicherheits- und Risikomanagement
- Sicherheit der Vermögenswerte
- Sicherheitsarchitektur und -technik
- Kommunikations- und Netzwerksicherheit
- Identitäts- und Zugriffsverwaltung
- Sicherheitsbewertung und -prüfung
- Sicherheitsabläufe
- Softwareentwicklungssicherheit

Im dritten und letzten Teil gehen wir einzelne Sicherheitsbedenken nach. Dabei analysieren wir vergangene Attacken und besprechen die Vorgeensweise von Day0 vulnerabilities bis zur schliessung des Lecks (zB: die Log4j- oder Shellshock-Sicherheitslücke).

## Angaben zum Transfer der erworbenen Kompetenzen 

- Teil 1: Lesestoff, Einführung, Gruppenarbeit und Diskussion
- Teil 2: Selbstständiger Vertiefungsarbeit (1-2er Gruppen) mit anschliessender Gruppenpräsentation
- Teil 3: Gemeinsame analyse von 2-3 Sicherehitslücken. Selbstständige Vertiefung für weitere

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
