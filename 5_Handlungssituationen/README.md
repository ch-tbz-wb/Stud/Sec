# Handlungsziele und Handlungssituationen 

Zu jedem einzelnen Handlungsziel aus der Modulidentifikation wird eine realistische, beispielhafte und konkrete Handlungssituation beschrieben, die zeigt, wie die Kompetenzen in der Praxis angewendet werden.

### B9.1 ICT-Sicherheitskonzepte zur Gewährleistung der ICT-Sicherheit wie Datenschutz, Datensicherheit und Verfügbarkeit einhalten, umsetzen und unterhalten 

Ich kann das CIA-Dreieck (Confidentiality, Integrity, Availability) erklären und weiss, dass sich diese Eigenschaften gegenseitig beinflussen.
Ich kenne Möglichkeiten um einzelne CIA Eigneschaften zu beinflussen und kenne deren auswirkungen auf die anderen Eigenschaften. und MAssnahmen um die Platzierung von IT-Systemenum die Sicherheit im CIA-Dreick  
Ich kann IT System andhand des CIA Dreiecks einordnen.

### 2. B9.3 Sicherheitsrelevante Bausteine vernetzter ICTInfrastrukturen identifizieren, die Gefährdungslage beurteilen und geeignete organisatorische, personelle, infrastrukturelle und technische Schutzmassnahmen ableiten

Ich kenne die verschiedenen Prozesse und Tätigkeiten, welche die Sicherehit von IT-Systemen beinflussen können. (CISSP Dimensionen)  
Ich kenne Mittel und Methoden um einzelne Bausteine zu verbessern. 