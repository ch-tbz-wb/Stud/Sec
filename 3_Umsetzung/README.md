# Umsetzung

 - Bereich: Datensicherheit gewährleisten
 - Semester: 3

## Lektionen

 * Präsenz: 20
 * Virtuell: 20
 * Selbststudium: 20

## Lernziele
 - Können Massnahmen zur Gewährleistung der Datensicherheit in ihren Services identifizieren und umsetzen.
 - Verstehen das CIA-Dreieck (Confidentiality, Integrity, Availability) und können dessen Bedeutung für IT-Prozesse erläutern und anwenden.
 - Erkennen die Grenzen absoluter Sicherheit und können die Notwendigkeit von Risikomanagement in der IT-Sicherheit diskutieren.
 - Können aktuelle Sicherheitsvorfälle analysieren und bewerten, welche Massnahmen zur Verbesserung der Sicherheitslage beigetragen hätten.
 - Haben einen Überblick über die 8 Dimensionen der CISSP-Zertifizierung und verstehen, wie diese zusammenwirken, um umfassende IT-Sicherheit zu gewährleisten.
 - Sind fähig, Sicherheits- und Risikomanagementstrategien zu entwickeln und umzusetzen, um den Schutz von IT-Vermögenswerten zu verbessern.
 - Können Sicherheitsarchitekturen und -techniken evaluieren und anwenden, um die Sicherheit der Kommunikation und Netzwerke zu gewährleisten.
 - Verstehen die Wichtigkeit der Identitäts- und Zugriffsverwaltung und können entsprechende Sicherheitsmechanismen implementieren.
 - Sind in der Lage, Sicherheitsbewertungen und -prüfungen durchzuführen, um Schwachstellen zu identifizieren und Gegenmassnahmen zu ergreifen.
 - Können die Prinzipien der Softwareentwicklungssicherheit anwenden, um die Sicherheit von Anwendungen von der Entwicklung bis zur Auslieferung zu gewährleisten.
 - Haben die Fähigkeit, auf Sicherheitsbedenken und -vorfälle, einschliesslich der Analyse und Behebung von Zero-Day-Vulnerabilities, angemessen zu reagieren.

## Voraussetzungen

- Kenntnisse von Cryptoprotokollen
- Kenntnisse der Telekommunikation im Netzwerk

## Technik

Tools zur Überprüfung von Datensicherheit / Penetration Testing

## Methoden

Vorträge, Einzel- und Gruppenarbeiten, Projektarbeit

## Schlüsselbegriffe

CIA-Dreieck, Schutzziele, Verschlüsselung, Authentifizierung, Integrität, Vertraulichkeit, Verfügbarkeit, Zugriffskontrolle

## Lehr- und Lernformen: 

Lehrervorträge, Lehrgespräche, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Repository




